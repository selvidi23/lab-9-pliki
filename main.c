#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// Proszę napisać program, który pozwala wykonać następujące operacje na domyślnym pliku tekstowym:
// Odczyt i wyświetlenie pierwszej linijki
// Zapis do pliku linii tekstu podanej przez użytkownika
// Dopisanie do pliku linii tekstu podanej przez użytkownika
// Odczyt i wyświetlenie n kolejnych linii tekstu z pliku (n podaje użytkownik)
// Odczyt i wyświetlenie całego pliku
// Odczyt i wyświetlenie pojedynczej n-tej linii (n podaje użytkownik)
// Wyświetlenie listy pozycji, na których zaczynają się poszczególne linie
// Odczyt i wyświetlenie pojedynczej n-tej linii (z wykorzystaniem informacji z punktu 7)
// Wybór przez użytkownika pliku, na którym będą wykonywane powyższe operacje

char path_file_main[] = "plik.txt";
FILE *file;
char buffor[1000];
int sw=0;
void read_First_Line(char path_file[]){

    if((file = fopen(path_file,"r"))!=NULL){
        fscanf(file,"%[^\n]",buffor);
        printf("%s  \n",buffor);
        fclose(file);
    }else{
        printf("Bład odczytu pliku\n");
    }
}
void write_Line_To_File(char path_file[],char line[]){
     
    if((file = fopen(path_file,"w"))!=NULL){
        fprintf(file,"%s",line);
        fclose(file);
    }else{
        printf("Bład odczytu pliku\n");
    }
 }

void append_Line_To_File(char path_file[],char line[]){
   
    if((file = fopen(path_file,"a"))!=NULL){
        fprintf(file,"%s","\n");
        fprintf(file,"%s",line);
        fclose(file);
    }else{
        printf("Bład odczytu pliku\n");
    }
}
void read_N_Lines(char path_file[],int n){
    char c;
    int counter=0;
    if((file = fopen(path_file,"r"))!=NULL){
       
        while((c=fgetc(file))!=EOF){
            if(counter<n){
                printf("%c",c);
            }
            if(c == '\n'){
                counter++;
            }
            
        }
        fclose(file);
    }else{
        printf("Blad odczytu pliku\n");
    }
}

void read_File(char path_file[]){
    char c;
    if((file = fopen(path_file,"r"))!=NULL){
       
        while((c=fgetc(file))!=EOF){
            printf("%c",c);
        }
        fclose(file);
    }else{
        printf("Blad odczytu pliku\n");
    }
}

void read_Single_N_Line(char path_file[],int n){
    char c;
    int counter=1,test=0;
    if((file = fopen(path_file,"r"))!=NULL){
       
        while((c=fgetc(file))!=EOF){
            if(counter==n){
                printf("%c",c);
                test =1;
            }
            if(c == '\n'){
                counter++;
            }
            
        }
        fclose(file);
    }else{
        printf("Blad odczytu pliku\n");
    }
    if(test ==0){
        printf("Zly indeks linii \n");
    }
}
void print_Index_File(int index){
    int counter =0;   
    if((file = fopen(path_file_main, "r"))!=NULL){
            while(!feof(file)){
                long long int position = ftell(file);
                fscanf(file,"%[^\n]s",buffor);
                if(sw == 0){
                    printf("%d(%lld): %s\n",counter,position,buffor);
                }else if(index == counter){
                    printf("%d(%lld): %s\n",counter,position,buffor);
                }
                
                fscanf(file,"%[\n]s",buffor);
                counter++;
            }
            fclose(file);
    }

}

void print_Line_With_inex(int index){
    if((file = fopen(path_file_main, "r"))!=NULL){

        fseek (file , index , 0);
        fscanf(file,"%[^\n]s",buffor);
        printf("%s  \n",buffor);

    }
    fclose(file);
}

void change_Path_To_File(char path_file[]){
    strcpy(path_file_main, path_file);
}

int menu(){
  
    int liczba;
    printf("\n");
    printf("Wybierz opcje \n");
    printf(" \n");

    printf("1. Odczyt i wyswietlenie perwszej linii tekstu  \n");
    printf("2. Zapis linii tekstu podanej przez uzytkownika do pliku \n");
    printf("3. Dopisanie do pliku linii tekstu   \n");
    printf("4. Odczyt i wyswietlenie n kolejnych linii tekstu  \n");
    printf("5. Odczyt i wyswietlenie calego pliku \n");
    printf("6. Odczyt i wyswietlenie pojedynczej n-tej linii   \n");
    printf("7. Wyswietlenie listy pozycji, na ktorych zaczynaja sie poszczegolne linie  \n");
    printf("8. Odczyt i wyswietlenie pojedynczej n-tej linii \n");
    printf("9. Wybor pliku przez użytkownika \n");

   
    printf("0. Koniec Programu \n");
    printf(" \n");

    scanf("%d",&liczba);

    return liczba;
}

int main(){
    int a,b;
    int select;
    char tmp[300];
        do{
        select = menu();
        switch (select)
        {
        case 1:
            read_First_Line(path_file_main);
            break;
        case 2:
            printf("Podaj tekst \n");
            scanf(" %[^\n]s",buffor);
            write_Line_To_File(path_file_main,buffor);
            break;
        case 3:
            printf("Podaj tekst \n");
            scanf(" %[^\n]s",buffor);
            append_Line_To_File(path_file_main,buffor);
            break;
        case 4:
            printf("Podaj ile linii chcesz odczytac \n");
            scanf("%d",&a);
            read_N_Lines(path_file_main,a);
            break;
        case 5:
            read_File(path_file_main);
            break;
        case 6:
            printf("Podaj numer linii \n");
            scanf("%d",&a);
            read_Single_N_Line(path_file_main,a);

            break;
        case 7:
            print_Index_File(0);
            break;
        case 8:
            printf("Podaj indek\n");
            scanf("%d",&a);
            print_Line_With_inex(a);
            break;
        case 9:
            printf("Podaj nowa sciezke pliku \n");
            scanf(" %[^\n]s",tmp);
            change_Path_To_File(tmp);
            break;
        }
    }while(select!=0); 
    return 0;
}