# README #

// Proszę napisać program, który pozwala wykonać następujące operacje na domyślnym pliku tekstowym:
// Odczyt i wyświetlenie pierwszej linijki
// Zapis do pliku linii tekstu podanej przez użytkownika
// Dopisanie do pliku linii tekstu podanej przez użytkownika
// Odczyt i wyświetlenie n kolejnych linii tekstu z pliku (n podaje użytkownik)
// Odczyt i wyświetlenie całego pliku
// Odczyt i wyświetlenie pojedynczej n-tej linii (n podaje użytkownik)
// Wyświetlenie listy pozycji, na których zaczynają się poszczególne linie
// Odczyt i wyświetlenie pojedynczej n-tej linii (z wykorzystaniem informacji z punktu 7)
// Wybór przez użytkownika pliku, na którym będą wykonywane powyższe operacje
